# 20230520 第二次作业 PrepareStatement

（将MySQL数据添加至Java集合）

```Java
package java与数据库的连接.封装;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author zhc
 */
public class test {
    public static void main(String[] args) {
        //获取连接
        Driver.conn();
        //注入SQL语句
        Driver.query("select * from student");
        //关闭连接
        Driver.close();
        //查看集合
        Driver.show();
    }
}
```

```Java
package java与数据库的连接.封装;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author zhc
 */
public class Driver {
    private static final String Driver = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/student_db?useSSL=false&unicode=true&characterEncoding=utf8";
    private static final String user = "root";
    private static final String paw = "root";
    int id;
    String name;
    String sex;

    static void show() {
        for (int i = 0; i < list.size(); i++) {
            Driver driver = list.get(i);
            int id = driver.id;
            String name = driver.name;
            String sex = driver.sex;
            System.out.println("ID=" + id + ",name=" + name + ",sex=" + sex);
        }
    }

    //注册驱动
    static {
        System.out.println("注册驱动中...");
        try {
            Class.forName(Driver);
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册失败");
        }
    }

    public static Connection conn() {
        System.out.println("连接数据库中...");
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, user, paw);
        } catch (SQLException e) {
            System.out.println("数据库连接失败");
        }
        return con;
    }

    static ArrayList<Driver> list = new ArrayList<>();

    public static void query(String sql, String... keys) {
        System.out.println("查询中...");
        ResultSet rs = null;
        int j = 0;
        try {
            PreparedStatement pst = conn().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setString((i + 1), keys[i]);
            }
            rs = pst.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String sex = rs.getString(3);
                System.out.printf("ID=%d,name=%s,sex=%s\n", id, name, sex);
                Driver driver = new Driver();
                driver.id = id;
                driver.name = name;
                driver.sex = sex;
                list.add(j++, driver);
            }
        } catch (SQLException e) {
            System.out.println("查询失败");
        }
    }

    public static void update(String sql, String... keys) {
        System.out.println("更新中...");
        try {
            PreparedStatement pst = conn().prepareStatement(sql, keys);
            int i = pst.executeUpdate(sql);
            if (i > 0) {
                System.out.printf("变化了%d条\n", i);
            } else {
                System.out.printf("更新失败.变化%d条\n", i);
            }
        } catch (SQLException e) {
            System.out.println("更新失败");
        }
    }

    public static void close() {
        System.out.println("释放资源中...");
        try {
            conn().close();
        } catch (SQLException e) {
            System.out.println("释放资源失败");
        }
        System.out.println("释放资源成功");
    }

}
```