# 20230526 第四次作业

 **通过网页修改数据库**

```java

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Insert", value = "/Insert")
public class Insert extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        String sql = "insert into t1 values(?,?,?)";
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        System.out.println(id);
        System.out.println(name);
        System.out.println(sex);
        int i = Driver.update(sql, id, name, sex);
        if (i > 0) {
            writer.write("插入成功，变化了" + i + "条数据");
        } else {
            writer.write("插入失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
```

```java
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "delete", value = "/delete")
public class delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();

        String sql = "delete from t1 where id=?";
        String id = request.getParameter("id");
        int i = Driver.update(sql, id);
        if (i > 0) {
            writer.write("删除成功");
        } else {
            writer.write("删除失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

```java
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/update")
public class Update extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();

        String sql = "update t1 set name=?,sex=? where id=?";
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        int i = Driver.update(sql, name, sex, id);
        System.out.println(name);
        System.out.println(sex);
        System.out.println(id);
        if (i > 0) {
            writer.write("修改成功");
        } else {
            writer.write("修改失败");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

```java
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Query", value = "/Query")
public class Query extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();

        String id = request.getParameter("qid");
        String sql = "select * from t1 where id like ?";
        ResultSet rs = Driver.query(sql, "%" + id + "%");
        System.out.println(sql);
        System.out.println(id);
        try {
            if (!rs.isBeforeFirst()) {
                writer.write("数据库中没有此ID的信息");
            } else {
                while (rs.next()) {
                    String Id = rs.getString(1);
                    String name = rs.getString(2);
                    String sex = rs.getString(3);
                    writer.write("\tID=" + Id +
                            "\tname=" + name +
                            "\tsex=" + sex);
                    writer.write("<br>");
                }
            }
        } catch (SQLException e) {
            writer.write("查询错误");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test</title>
</head>
<body>
<h1>插入</h1>
<form action="/Insert">
    编号：<input type="text" name="id" id="id"><br>
    姓名：<input type="text" name="name" id="name"><br>
    性别：<input type="text" name="sex" id="sex"><br>
    <input type="submit" value="插入">
</form>
<br><br>
<form action="/Query">
    <h1>查询</h1>
    请输入要查询的ID:<input type="text" name="qid" id="qid"><br>
    <input type="submit" value="查询">
</form>
<h1>修改</h1>
<form action="/update">
    请输入要修改的ID：<input type="text" name="id"><br>
    姓名：<input type="text" name="name"><br>
    性别：<input type="text" name="sex"><br>
    <input type="submit" value="修改">
</form>
<h1>删除</h1>
<form action="/delete">
    请输入要删除的ID：<input type="text" name="id"><br>
    <input type="submit" value="删除">
</form>
</body>
</html>
```