# 20230609 第七次作业 companyManager试卷

## bean包

### **list**类

```java
package Bean;

public class Dept {
    private int DeptID;
    private String DeptName;

    public Dept(int deptID, String deptName) {
        DeptID = deptID;
        DeptName = deptName;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "DeptID=" + DeptID +
                ", DeptName='" + DeptName + '\'' +
                '}';
    }

    public int getDeptID() {
        return DeptID;
    }

    public String getDeptName() {
        return DeptName;
    }

    public Dept() {
    }
}

```

### Emp类

```java
package Bean;

public class Emp {
    private int EmpID, Age, DeptID;
    private String EmpName, Sex, Tel, PassWord,Dep;

    public String getDep() {
        return Dep;
    }

    public Emp(String empID, String age, String deptID, String empName, String sex, String tel, String passWord, String dep) {
        EmpID = Integer.parseInt(empID);
        Age = Integer.parseInt(age);
        DeptID = Integer.parseInt(deptID);
        EmpName = empName;
        Sex = sex;
        Tel = tel;
        PassWord = passWord;
        Dep = dep;
    }

    public Emp(String empID, String age, String deptID, String empName, String sex, String tel, String passWord) {
    }

    @Override
    public String toString() {
        return "Emp{" +
                "EmpID=" + EmpID +
                ", Age=" + Age +
                ", DeptID=" + DeptID +
                ", EmpName='" + EmpName + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Tel='" + Tel + '\'' +
                ", PassWord='" + PassWord + '\'' +
                '}';
    }

    public int getEmpID() {
        return EmpID;
    }

    public int getAge() {
        return Age;
    }

    public int getDeptID() {
        return DeptID;
    }

    public String getEmpName() {
        return EmpName;
    }

    public String getSex() {
        return Sex;
    }

    public String getTel() {
        return Tel;
    }

    public String getPassWord() {
        return PassWord;
    }

    public Emp() {
    }

    public Emp(int empID, int age, int deptID, String empName, String sex, String tel, String passWord) {
        EmpID = empID;
        Age = age;
        DeptID = deptID;
        EmpName = empName;
        Sex = sex;
        Tel = tel;
        PassWord = passWord;
    }
}

```

## utils包

### DBUtil类

```java
package Utils;

import com.mysql.jdbc.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBUtil {
    private static final String Driver = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql:///companyManager?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String Username = "root";
    private static final String Paw = "root";

    static {
        try {
            Class.forName(Driver);
        } catch (ClassNotFoundException e) {
            System.out.println("注册驱动失败");
            e.printStackTrace();
        }
    }

    public static Connection conn() {
        Connection connection = null;
        try {
            connection = (Connection) DriverManager.getConnection(URL, Username, Paw);
        } catch (SQLException e) {
            System.out.println("连接失败");
            e.printStackTrace();
        }
        return connection;
    }

    public static ResultSet query(String sql, String... keys) {
        ResultSet rs = null;
        try {
            PreparedStatement pst = conn().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setString((i + 1), "%" + keys[i] + "%");
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            System.out.println("查询异常");
            e.printStackTrace();
        }
        return rs;
    }

    public static int update(String sql, Object... keys) {
        int num = 0;
        try {
            PreparedStatement pst = conn().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("更新异常");
            e.printStackTrace();
        }
        return num;
    }
}

```

## dao包

### Dao类

```java
package Dao;

import Utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Dao {
    private static String sql;

    public static ArrayList<Bean.Emp> query() {
        sql = "select * from emp e,dept d where d.DeptID=e.DeptID";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Bean.Emp> list = new ArrayList<>();
        try {
            while (rs.next()) {
                String empID = rs.getString("EmpID");
                String empName = rs.getString("EmpName");
                String sex = rs.getString("Sex");
                String age = rs.getString("Age");
                String tel = rs.getString("Tel");
                String passWord = rs.getString("PassWord");
                String deptID = rs.getString("DeptID");
                String DeptName = rs.getString("DeptName");

                list.add(new Bean.Emp(empID, age, deptID, empName, sex, tel, passWord, DeptName));
                System.out.println(list);
            }
        } catch (SQLException e) {
            System.out.println("打印异常");
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<Bean.Emp> query2(String... Keys) {
        sql = "select * from emp e,dept d where d.DeptID=e.DeptID && EmpName like ?";
        ResultSet rs = DBUtil.query(sql, Keys);
        ArrayList<Bean.Emp> list = new ArrayList<>();
        try {
            while (rs.next()) {
                String empID = rs.getString("EmpID");
                String empName = rs.getString("EmpName");
                String sex = rs.getString("Sex");
                String age = rs.getString("Age");
                String tel = rs.getString("Tel");
                String passWord = rs.getString("PassWord");
                String deptID = rs.getString("DeptID");
                String DeptName = rs.getString("DeptName");

                list.add(new Bean.Emp(empID, age, deptID, empName, sex, tel, passWord, DeptName));
                System.out.println(list);
            }
        } catch (SQLException e) {
            System.out.println("打印异常");
            e.printStackTrace();
        }
        return list;

    }

//    public static ArrayList<Bean.Dept> query2() {
//        sql = "select * from houseType";
//        ResultSet rs = DBUtil.query(sql);
//        ArrayList<Bean.Dept> list = new ArrayList<>();
//        try {
//            while (rs.next()) {
//
//                list.add(new Bean.Dept());
//            }
//        } catch (SQLException e) {
//            System.out.println("打印异常");
//            e.printStackTrace();
//        }
//        return list;
//    }


}

```

## servlet包

### list类

```java
package Servlet;


import Bean.Emp;
import Dao.Dao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "list", value = "/companyManager/*")
public class list extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        String path = request.getPathInfo();
        String name = null;
        if (path == null || path.equals("/list")) {
            ArrayList<Bean.Emp> list = Dao.query();
            request.setAttribute("list", list);
            request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request, response);
            System.out.println("传值成功");
        } else if (path.equals("/query")) {
            name = request.getParameter("name");
            System.out.println(name);
            ArrayList<Bean.Emp> list = Dao.query2(name);
            request.setAttribute("list", list);
            request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request, response);
            System.out.println("传值成功");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}

```

## 网页

### 显示页面list

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-08
  Time: 17:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Info</title>
    <style>
        table, td, th {
            border: black solid 1px;
            border-collapse: collapse;
        }

        h1, table {
            margin: auto 100px;
            text-align: center;
        }

        td, th {
            width: 500px;
        }
    </style>
</head>
<body>
<div style="width: 400px;margin: auto 400px">
    <form action="/companyManager/query">
        姓名：<input type="text" name="name">
        <input type="submit" value="查询" style="width: 80px;margin-left: 10px">
    </form>
</div>
<table>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>电话</th>
        <th>所属部门</th>
    </tr>
    <c:forEach items="${list}" var="emp">
        <tr>
            <td>${emp.empID}</td>
            <td>${emp.empName}</td>
            <td>${emp.sex}</td>
            <td>${emp.age}</td>
            <td>${emp.tel}</td>
            <td>${emp.dep}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>

```

### 
